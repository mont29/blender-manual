.. index:: Editors; Compositor

**********
Compositor
**********

The Compositor lets you manage :doc:`nodes </interface/controls/nodes/introduction>`
for compositing.

.. figure:: /images/compositing_types_distort_map-uv_example-2.png

   Nodes in the Compositor.

The use of the Compositor is explained in :doc:`/compositing/index`.
