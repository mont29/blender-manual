.. _bpy.types.Strip:
.. _bpy.ops.sequencer:

#################
  Video Editing
#################

.. toctree::
   :maxdepth: 2

   introduction.rst
   setup/index.rst
   edit/index.rst
