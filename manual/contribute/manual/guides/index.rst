
##############
  Guidelines
##############

.. toctree::
   :maxdepth: 1

   writing_guide.rst
   markup_guide.rst
   commit_guide.rst
   templates.rst
   maintenance_guide.rst
