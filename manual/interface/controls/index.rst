
###############
  UI Elements
###############

.. toctree::
   :maxdepth: 1

   buttons/buttons.rst
   buttons/fields.rst
   buttons/menus.rst
   buttons/eyedropper.rst
   buttons/decorators.rst
   templates/data_block.rst
   templates/list_view.rst
   templates/color_picker.rst
   templates/color_ramp.rst
   templates/color_palette.rst
   templates/curve.rst
