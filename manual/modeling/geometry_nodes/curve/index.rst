
###############
  Curve Nodes
###############

Nodes that only operate on curves.

.. note::
   If any modifiers precede the Geometry Nodes modifier,
   the curve will be seen as a mesh internally, and the Curve Nodes won't work.
   To get around this, first run the geometry through a
   :doc:`/modeling/geometry_nodes/mesh/operations/mesh_to_curve`.

.. toctree::
   :maxdepth: 2

   Read <read/index.rst>
   Sample <sample/index.rst>
   Write <write/index.rst>

-----

.. toctree::
   :maxdepth: 2

   Operations <operations/index.rst>
   Primitives <primitives/index.rst>
   Topology <topology/index.rst>
