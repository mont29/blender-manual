
####################
  Deprecated Nodes
####################

These nodes have been made deprecated, meaning they will be removed in a future version and **should not be used**.

.. toctree::
   :maxdepth: 1

   align_euler_to_vector.rst
   rotate_euler.rst
