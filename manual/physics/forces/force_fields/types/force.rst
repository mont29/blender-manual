.. index:: Force Fields; Force

*****
Force
*****

.. reference::

   :Panel:     :menuselection:`Physics --> Force Fields`
   :Type:      Force

.. figure:: /images/physics_forces_force-fields_types_force_visualzation.png

   Force field visualization.

The *Force* field is the simplest of the fields. It gives a constant force away from
(positive strength) or towards (negative strength) the object's origin.


Example
=======

.. peertube:: ea6ce3d3-b1da-4173-904f-6c4c0273eac4
