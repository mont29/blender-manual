
###########
  Brushes
###########

Brushes for *Sculpt Mode* bundled in the *Essentials* library.

.. toctree::
   :maxdepth: 1

   brushes.rst
   draw.rst
   draw_sharp.rst
   clay.rst
   clay_strips.rst
   clay_thumb.rst
   layer.rst
   inflate.rst
   blob.rst
   crease.rst
   smooth.rst
   flatten.rst
   fill.rst
   scrape.rst
   plane.rst
   multiplane_scrape.rst
   pinch.rst
   grab.rst
   elastic_deform.rst
   snake_hook.rst
   thumb.rst
   pose.rst
   nudge.rst
   rotate.rst
   slide_relax.rst
   boundary.rst
   cloth.rst
   simplify.rst
   mask.rst
   draw_facesets.rst
   multires_displacement_eraser.rst
   multires_displacement_smear.rst
   paint.rst
   smear.rst
