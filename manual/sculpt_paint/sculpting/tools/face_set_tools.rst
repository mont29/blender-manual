
**********************
Face Set Gesture Tools
**********************

.. reference::

   :Mode:      Sculpt Mode

Face Set gesture tools apply a single new Face Set to all faces within the selection area.

All Face Set gesture tools can be activated in the Toolbar and are comprised of the following:

.. _tool-box-face-set:

Box Face Set
============

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Box Face Set`


Creates a new :doc:`Face Set </sculpt_paint/sculpting/editing/face_sets>`
based on a :ref:`box gesture <gesture-tool-box>`.

.. _tool-lasso-face-set:

Lasso Face Set
==============

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Lasso Face Set`

Creates a new :doc:`Face Set </sculpt_paint/sculpting/editing/face_sets>`
based on a :ref:`lasso gesture <gesture-tool-lasso>`.

.. _tool-line-face-set:

Line Face Set
=============

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Line Face Set`

Creates a new :doc:`Face Set </sculpt_paint/sculpting/editing/face_sets>`
based on a :ref:`line gesture <gesture-tool-line>`.

.. _tool-polyline-face-set:

Polyline Face Set
=================

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Polyline Face Set`

Creates a new :doc:`Face Set </sculpt_paint/sculpting/editing/face_sets>`
based on a :ref:`polyline gesture <gesture-tool-polyline>`.

Tool Settings
=============

Front Faces Only
   Only creates a face set on the faces that face towards the view.
