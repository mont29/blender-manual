*****
Brush
*****

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Brush`


Tool to use for any of the *Sculpt* mode brushes. Activating a brush from an asset shelf or brush
selector will also activate this tool for convenience.

See the list of :doc:`Essentials Brushes </sculpt_paint/sculpting/brushes/brushes>`
(based on available :doc:`Brush Types </sculpt_paint/sculpting/brushes/index>`) for more details.