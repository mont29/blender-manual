.. _bpy.types.ImagePaint:
.. _bpy.types.Material.paint:

#################
  Texture Paint
#################

.. toctree::
   :maxdepth: 2

   introduction.rst
   brushes.rst
   tool_settings/index.rst
