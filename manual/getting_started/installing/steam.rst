
*********************
Installing from Steam
*********************

Steam is a software distribution platform. Blender can be downloaded and updated using the Steam client by following
the steps described below on Linux, macOS, or Windows.

Download and install the `Steam client <https://store.steampowered.com/>`__ for your operating system.

Once it is installed, open the client and login to your Steam account, or create an account if you don't already have
one. After logging in, navigate to the *Store* tab, search for "Blender", and press the green **Install** button.
Blender should now be available in the *Library* tab of the Steam client, where it can be launched. Optionally, a
shortcut can be added to your desktop by right-clicking on it in your library list.

.. seealso::

   When installing Blender from Steam on Linux and Windows, the ``.blend`` filename extension will not be
   automatically associated with Blender. To associate blend-files with Blender, see the processes described on the
   :doc:`Linux </getting_started/installing/linux>` and :doc:`Windows </getting_started/installing/windows>`
   installation pages.


Updating with Steam
===================

When an update for Blender is available on Steam, Steam will automatically download and apply the update for you.
