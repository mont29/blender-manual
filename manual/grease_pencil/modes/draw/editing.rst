******************
Drawing Operations
******************

Active Layer
============

.. reference::

   :Mode:      Draw Mode
   :Menu:      :menuselection:`Draw --> Active Layer`
   :Shortcut:  :kbd:`Y`

Select the active layer.


Animation
=========

.. reference::

   :Mode:      Draw Mode
   :Menu:      :menuselection:`Draw --> Animation`
   :Shortcut:  :kbd:`I`

The stroke animation operations are described in the :doc:`Animation </grease_pencil/animation/tools>` section.


Interpolate Sequence
====================

.. reference::

   :Mode:      Draw Mode
   :Menu:      :menuselection:`Draw --> Interpolate Sequence`

See :ref:`bpy.ops.grease_pencil.interpolate_sequence`.
