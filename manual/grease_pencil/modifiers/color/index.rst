
#################################
  Grease Pencil Color Modifiers
#################################

.. toctree::
   :maxdepth: 1

   hue_saturation.rst
   opacity.rst
   tint.rst
